(custom-set-variables
  '(inhibit-startup-screen t))

(setq-default truncate-lines t)

(setq-default indent-tabs-mode nil)
(setq tab-width 2)

(setq tab-width 4)
(column-number-mode 1)
(global-visual-line-mode 1)
(global-whitespace-mode 1)
;; see the apropos entry for whitespace-style
(setq
   whitespace-style
   '(face ; viz via faces
     trailing ; trailing blanks visualized
     lines-tail ; lines beyond
                ; whitespace-line-column
     space-before-tab
     space-after-tab
     newline ; lines with only blanks
     indentation ; spaces used for indent
                 ; when config wants tabs
     empty ; empty lines at beginning or end
     )
   whitespace-line-column 100 ; column at which
        ; whitespace-mode says the line is too long
        )

(fset 'yes-or-no-p 'y-or-n-p)

(show-paren-mode 1)
(setq show-paren-style 'expression)

(setq backup-directory-alist `(("." . "~/.saves")))

(setq backup-by-copying t)

(setq delete-old-versions t
  kept-new-versions 1
  kept-old-versions 1
  version-control t)

(ido-mode 1)

(global-set-key "\M-a" 'next-buffer)
(global-set-key "\M-e" 'previous-buffer)

(global-set-key "\M-p" (lambda () (interactive) (forward-line -5)))
(global-set-key "\M-n" (lambda () (interactive) (forward-line 5)))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'monokai t)

(defun user-file-search-upward (directory file)
  (interactive)
  (let ((parent-dir (file-truename (concat (file-name-directory directory) "../")))
        (current-path (if (not (string= (substring directory (- (length directory) 1)) "/"))
                         (concat directory "/" file)
                         (concat directory file))))
    (if (file-exists-p current-path)
        current-path
        (when (and (not (string= (file-truename directory) parent-dir))
                   (< (length parent-dir) (length (file-truename directory))))
          (user-file-search-upward parent-dir file)))))

(require 'yasnippet)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

(add-hook 'prog-mode-hook 'linum-mode)

;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
(setq lsp-keymap-prefix "C-l")

(use-package lsp-mode
    :hook (;; Add other modes here just like the first.
            (rust-mode . lsp)
            (go-mode . lsp))
    :commands lsp)

;; optionally
(use-package lsp-ui :commands lsp-ui-mode)
;; if you are helm user
(use-package helm-lsp :commands helm-lsp-workspace-symbol)

;; optionally if you want to use debugger
;; (use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

(use-package cargo-minor-mode
  :hook (
    (rust-mode . cargo-minor-mode)
  )
)
