(define-package "lsp-mode" "20200905.1835" "LSP mode"
  '((emacs "26.1")
    (dash "2.14.1")
    (dash-functional "2.14.1")
    (f "0.20.0")
    (ht "2.0")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0"))
  :commit "1805c9caa146f60a5d9109f2a14d3596a5d1228f" :keywords
  '("languages")
  :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
